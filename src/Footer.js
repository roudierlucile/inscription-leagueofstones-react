import React from 'react';
import logo from './logo.svg';
import './App.css';

class Footer extends React.Component {
  render(){
    return(
      <footer id="sticky-footer" class="py-4 bg-black text-white-50">
        <div class="text-center">
          <small>Copyright &copy; Roudier Lucile/Léo Laugier</small>
        </div>
      </footer>
    );
  }
}

export default Footer;
