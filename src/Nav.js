import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { FORM_ERROR } from 'final-form'
import './App.css';
//import Inscription from "./Inscription";

import './App.css';

class Nav extends React.Component {
  render(){
  return (
      <div>
        <nav className="navbar navbar-expand-sm bg-black navbar-dark">
          <a className="navbar-brand" href="#">League of stones</a>
          <ul className="navbar-nav">
            <Link to="/connexion">
            <li className="nav-item ">
              <a className="nav-link"> Connexion </a>
            </li>
            </Link>
            <Link to="/inscription">
            <li className="nav-item">
              <a className="nav-link">Inscription</a>
            </li>
            </Link>
          </ul>
        </nav>
    </div>

          );
  }
}
export default Nav;
