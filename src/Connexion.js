import React from 'react';
import {Form} from "react-final-form";
import logo from './logo.svg';
import './App.css';
import { Field } from "react-final-form";

class Connexion extends React.Component {
  constructor(props) {
    super(props);

    this.initialState = {
      pseudo: '',
      password: '',
      stateMess:'none',
      mess:'',
      connexion:false
    }

    this.state = this.initialState;
  }

  onSubmit(donnee) {
    const data = {
      username : donnee.pseudo,
      summonername : "",
      email : "",
      password : donnee.password
    }
    const url = "https://virtserver.swaggerhub.com/L3MIASHS/LOS/1.0.0/user";
    const configSend = {
      method : "PUT",
      body : JSON.stringify(data)
    };

    fetch(url, configSend)
    .then(result =>{
      if(result.status === 201){
          window.alert("Vous êtes connecté");
      }else{
        this.setState({connexion:true});
      //  this.setState({ mess: "Erreur de l'envoie du formulaire",stateMess:"initial" })
      }
      console.log(result);
    })
  }

  render() {
    return (

      <div className="contaner-fluid">
      <div className="row justify-content-center">
      <div className="col-xs-6 col-md-4">
      <div className="card mt-4 mb-4 bg-dark" >
      <h5 className="card-header bg-black text-white text-center py-4" >
        <strong>Connexion</strong>
      </h5>
      <div className="card-body text-white px-lg-5 pt-0">
      <Form onSubmit = {this.onSubmit}
        render = {({handleSubmit}) => (
          <form className = "form" onSubmit = {handleSubmit}>
            <div className = "form-group">
              <label>Pseudo</label>
              <Field
                name = "pseudo"
                component = "input"
                placeholder = "Pseudo"
                className = "form-control"
                required
              />
            </div>

            <div className="form-group">
              <label>Mot de passe</label>
              <Field
                name="password"
                component="input"
                type="password"
                placeholder="Mot de passe"
                className="form-control"
                required
              />
            </div>

            <button className="btn btn-outline-warning btn-rounded btn-block my-4 waves-effect z-depth-0" type = "submit">Connexion</button>
          </form>

          )
        }/>
            </div>

            <div className="alert alert-info" role="alert" style={{display:this.state.stateMess}}>
              {this.state.mess}
            </div>
          </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Connexion;
