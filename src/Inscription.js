import React from 'react';
import './App.css';
import { Form, Field } from 'react-final-form'
import {BrowserRouter as Router,Switch,  Route,Link,Redirect} from "react-router-dom";


class Inscription extends React.Component{
  constructor(props) {
    super(props);
    this.state = {username: '',
                  password: '',
                  pass2:'',
                  mess:'',
                  stateMess:'none'};

  }

  render(){
    const myOnSubmit =(formObj) => {

      const errors = {};
      console.log(formObj);

      const myBody = {
            username: formObj.username,
            summonername: '',
            email: "test@test.fr",
            password: formObj.password,
        }
      const url = 'https://virtserver.swaggerhub.com/L3MIASHS/LOS/1.0.0/user';
      const configRecup = { method: 'PUT',
                            body: JSON.stringify(myBody)
                          }
      if(formObj.pass1 === formObj.pass2){
      fetch(url,configRecup)
      .then(result =>{
        if(result.status === 201){
            this.setState({ mess: "Tout s'est bien déroulé" })
            window.alert("Votre compte a été crée");
            return <Redirect to='/home'  />

        }else if (result.status===409) {

          this.setState({ mess: "Utilisateur déja existant", stateMess:"initial" })
        }else{
          this.setState({ mess: "Erreur de l'envoie du formulaire",stateMess:"initial" })
        }
        console.log(result.status) })
      }
      else{
          this.setState({ mess: "Erreur dans la confirmation du mot de passe",stateMess:"initial" })
      }
    };

    return(
      <div className="container-fluid mb-4">
      <div className="row justify-content-center">
      <div className="col-xs-6 col-md-4">
      <div className="card mt-4 mb-4   bg-blackCard " >
      <h5 className="card-header bg-dark text-white text-center py-4" >
        <strong>Inscription</strong>
      </h5>
      <div className="card-body bg-dark text-white px-lg-5 pt-2">
      <Form onSubmit={myOnSubmit} render={({handleSubmit})=>
        (<form onSubmit={handleSubmit} className="form-signin text-center">
           <div className="md-form text-white">
            <label for="pseudo" className="text-white">Pseudo</label>
              <Field name="Pseudo">
                {({ input }) => (
                  <input
                    placeholder="Pseudo"
                    type="text"
                    id="pseudo"
                    value={this.state.pseudo}
                    onChange={this.handleChange}
                    required
                    className="form-control"
                    {...input}
                  />
                )}
              </Field>
             </div>
             <div className="md-form text-white">
              <label for="mdp">Mot de Passe</label>
              <Field name="pass1">
                {({ input }) => (
                  <input
                    placeholder="Votre mot de passe"
                    type="password"
                    id="mdp"
                    className="form-control"
                    value={this.state.pass1}
                    required
                    {...input}
                  />
                )}
              </Field>
              </div>
              <div className="md-form">
              <label for="mdp2">Confirmer votre mot de passe</label>
              <Field name="pass2">
                {({ input }) => (
                  <input
                    placeholder="Confirmer votre mot de passe"
                    type="password"
                    id="mdp2"
                    className="form-control"
                    required
                    value={this.state.pass2}
                    {...input}
                  />
                )}
              </Field>
              </div>

               <button type="submit" className="btn btn-outline-warning btn-rounded btn-block my-4 waves-effect z-depth-0">Valider</button>

            </form>

      )}/>

        <div className="alert alert-danger" role="alert" style={{display:this.state.stateMess}}>
          {this.state.mess}
        </div>
        </div>
      </div>
    </div>
    </div>
    </div>
    );
  }
}

export default Inscription;
