import React from 'react';
import logo from './logo.svg';
import './App.css';
import Inscription from './Inscription';
import Nav from './Nav';
import Footer from './Footer';
import Connexion from './Connexion';
import {BrowserRouter as Router,Switch,  Route} from "react-router-dom";

function App() {
  return (

      <div className="App">
      <Router>
        <Nav />
        <Switch>
          <Route path="/Connexion" exact component={Connexion} />
          <Route path="/Inscription" exact component={Inscription} />
        </Switch>
      </Router>

      </div>



  );
}


export default App;
